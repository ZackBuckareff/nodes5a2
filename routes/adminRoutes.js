const path = require('path');
const express = require('express');

const utils = require('../util/utils');

const router = express.Router();

router.get('/', (req, res,next) => {
  res.sendFile(path.join(utils.rootpath, 'views', 'admin', 'admin.html'));
});

router.get('/admin-list', (req, res, next) => {
  res.sendFile(path.join(utils.rootpath, 'views', 'admin', 'admin-list.html'));
});

router.get('/add-admin', (req, res, next) => {
  res.sendFile(path.join(utils.rootpath, 'views', 'admin', 'add-admin.html'));
});

router.post('add-admin', (req, res, next) => {
  res.redirect('/admin/admin-list');
});

module.exports = router;