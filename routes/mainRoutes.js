const path = require('path');
const express = require('express');

const utils = require('../util/utils');

const router = express.Router();

router.get('/', (req, res,next) => {
  res.redirect('/home');
});

router.get('/home', (req, res, next) => {
  res.sendFile(path.join(utils.rootpath, 'views', 'main', 'home.html'));
});

module.exports = router;