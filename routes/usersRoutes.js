const path = require('path');
const express = require('express');

const utils = require('../util/utils');

const router = express.Router();

router.get('/', (req, res,next) => {
  res.sendFile(path.join(utils.rootpath, 'views', 'users', 'users.html'));
});

router.get('/user-list', (req, res, next) => {
  res.sendFile(path.join(utils.rootpath, 'views', 'users', 'user-list.html'));
});

router.get('/add-user', (req, res, next) => {
  res.sendFile(path.join(utils.rootpath, 'views', 'users', 'add-user.html'));
});

router.post('add-user', (req, res, next) => {
  res.redirect('/users/all-users')
})

module.exports = router;